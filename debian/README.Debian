Local UI

If you want bettercap web ui running on your computer, you’ll use the
http-ui caplet which will start the api.rest and http.server modules on
127.0.0.1.

Edit the default credentials in /usr/share/bettercap/caplets/http-ui.cap
and then start the ui with:

sudo bettercap -caplet http-ui

Open your browser to http://127.0.0.1/ and login using the credentials you
configured in the previous step.


Remote UI

If instead you’re running bettercap on another host, another machine with
a different IP address, you want to use the https-ui caplet in order for
the connection to the UI and the api to be protected by TLS. The caplet
will bind the modules on 0.0.0.0 and generate a self signed certificate
you can then allow in your browser.

Edit the default credentials in
/usr/share/bettercap/caplets/https-ui.cap and then start the ui
with:

sudo bettercap -caplet https-ui

Open your browser to https://<ip of the machine>/ and login using the
credentials you configured in the previous step.

More information here:
https://www.bettercap.org/usage/#web-ui

